package kr.co.hbilab.app;

public class BankImple implements Bank {
    String name;
    int money;
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setMoney(int money) {
        this.money = money;
    }
    
    @Override
    public void transfer() {
        System.out.println("["+name+"] 님 계좌에서 ["+money+"] 원 이체 합니다.");
    }
    
}
