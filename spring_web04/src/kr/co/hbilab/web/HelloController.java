package kr.co.hbilab.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 2015. 5. 29.
 * @author user
 * 
 */
public class HelloController implements Hello, Controller{

    String name;
    
    
    
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1)
            throws Exception {
        ModelAndView model = new ModelAndView();
        model.addObject("msg", sayHello());
        model.setViewName("hello");
        return model;
    }

    @Override
    public String sayHello() {
        return "안녕하세요 " + name;
    }
    
}
