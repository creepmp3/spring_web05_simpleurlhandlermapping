package kr.co.hbilab.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 2015. 5. 29.
 * @author KDY
 * 
 */
public class Hicontroller implements Controller{

    @Override
    public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse resp)
            throws Exception {
        
        ModelAndView model = new ModelAndView("hi", "msg", "안녕하세요");
        model.addObject("data", req.getParameter("data"));
        return model;
    }
    
}
